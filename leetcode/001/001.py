#!/usr/bin/env python3

import sys

def n_squared_solution(nums, target):
	print("O(n^2) solution:")
	for x in range(0,len(nums)):
		for y in range(1,len(nums)):
			if (nums[x] + nums[y]) == target:
				print("({}; {})".format(x, y))
				return

def n_solution(nums, target):
	print("O(n) solution:")
	d = dict()
	for x in range(0,len(nums)):
		if nums[x] not in d:
			d[nums[x]] = x
		else:
			if type(d[nums[x]]) != list:
				d[nums[x]] = [d[nums[x]]]
			d[nums[x]].append(x)
	
	for k in d:
		if ((target - k) in d):
			if (d[target - k] != d[k]):
				print("({}; {})".format(d[k], d[target - k]))
				return
			elif (target - k) == k and type(d[k]) == list and len(d[k]) > 1:
				print("({}; {})".format(d[k][0], d[k][1]))
				return

def main():
	nums = sys.stdin.readline()
	nums = [int(n) for n in nums.split(' ')]

	target = int(sys.stdin.readline())

	n_squared_solution(nums, target)
	n_solution(nums, target)

if __name__ == "__main__":
	main()
