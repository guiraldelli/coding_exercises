import sys

def main():
	s = input()
	stack = [0, 0, 0]
	for c in s:
		if c == "(":
			stack[0] += 1
		elif c == ")":
			stack[0] -= 1
		elif c == "[":
			stack[1] += 1
		elif c == "]":
			stack[1] -= 1
		elif c == "{":
			stack[1] += 1
		elif c == "}":
			stack[1] -= 1
		else:
			sys.err.write("Symbol '{}' is not valid.", c)
			exit(1)
	print(str(all([x == 0 for x in stack])).lower())

if __name__ == "__main__":
	main()
