#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Trick to use the array as a hash-map for characters. */
#define POSITION_OF(x) ((x) - 'a')

enum {
	  MAP_LENGTH = (unsigned int)('z' - 'a')
	, PATTERN_MAX_LENGTH = 300
	, TEXT_MAX_LENGTH = 3000
};

int
main()
{
	char pattern[PATTERN_MAX_LENGTH] = {0};
	char text[TEXT_MAX_LENGTH] = {0};

	if (fgets(pattern, PATTERN_MAX_LENGTH, stdin) == NULL) {
		fprintf(stderr, "Could not read the pattern string.\n");
		exit(-1);
	}
        pattern[strlen(pattern) - 1] = '\0'; /* Trim the line-feed. */

	if (fgets(text, TEXT_MAX_LENGTH, stdin) == NULL) {
		fprintf(stderr, "Could not read the text string.\n");
		exit(-1);
	}
        text[strlen(text) - 1] = '\0'; /* Trim the line-feed. */

	const char* SEPARATORS = " \t\n\v";
	char* word = NULL;
	word = strtok(text, SEPARATORS);

	char* map[MAP_LENGTH] = {0};
	for (char* p = pattern; *p != '\0'; p++) {
                /* Pattern letter wasn't seem before. */
		if (map[POSITION_OF(*p)] == NULL && word != NULL) {
			map[POSITION_OF(*p)] = word;
		}
		else {
			if (word == NULL
			    || strcmp(word, map[POSITION_OF(*p)]) != 0) {
				printf("false\n");
				return 0;
			}
		}
		word = strtok(NULL, SEPARATORS);
	}
	if (word != NULL) {
		printf("false\n");
	}
	else {
		printf("true\n");
	}

	return 0;
}

